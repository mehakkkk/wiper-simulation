var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
let l1;
let l2;
let C;
let Wiper;
let Ignit;
var isinitialized = false;
var started = false;
var color1 = "grey";
var color2 = "grey";
var color;
function start() {
    if (!isinitialized) {
        isinitialized = true;
        let p1 = new Geometry.point(400, 300);
        let p2 = new Geometry.point(500, 300);
        //wiper lines
        l1 = new Geometry.line(p1, p2, context);
        let p3 = new Geometry.point(400, 350);
        let p4 = new Geometry.point(500, 350);
        l2 = new Geometry.line(p3, p4, context);
        let c1 = new Geometry.point(320 + 15, 325);
        let a1 = new Geometry.point(320, 325);
        C = new Geometry.circle(c1, a1, 4, context);
    }
    drawall();
}
function drawall() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    drawcircle(320, 325, 20, 0, 2 * Math.PI, "grey");
    drawcircle(50, 300, 20, 0, 2 * Math.PI, color1);
    drawcircle(50, 350, 20, 0, 2 * Math.PI, color2);
    context.fillStyle = "black";
    context.font = "10px Arial";
    context.fillText("Ignition on", 20, 270, 25);
    context.fillText("Wiper on", 10, 330, 25);
    context.fillText("ON", 40, 300, 35);
    context.fillText("ON", 40, 350, 35);
    context.beginPath();
    context.moveTo(400, 250);
    context.lineTo(400, 400);
    context.fillStyle = "blue";
    context.lineWidth = 10;
    context.lineWidth = 1;
    context.stroke();
    color = "black";
    gate(color);
    started = false;
}
function animate() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    drawcircle(320, 325, 20, 0, 2 * Math.PI, "grey");
    drawcircle(50, 300, 20, 0, 2 * Math.PI, color1);
    drawcircle(50, 350, 20, 0, 2 * Math.PI, color2);
    context.fillStyle = "black";
    context.font = "10px Arial";
    context.fillText("Ignition on", 20, 270, 25);
    context.fillText("Wiper on", 10, 330, 25);
    context.fillText("OFF", 40, 300, 35);
    context.fillText("OFF", 40, 350, 35);
    context.beginPath();
    context.moveTo(400, 250);
    context.lineTo(400, 400);
    context.strokeStyle = "blue";
    context.lineWidth = 10;
    context.stroke();
    context.strokeStyle = "red";
    l1.updateangle();
    l2.updateangle();
    l1.draw("red");
    l2.draw("red");
    context.strokeStyle = "yellow";
    C.draw();
    C.updateangle();
    color = "green";
    gate(color);
    if (started)
        window.requestAnimationFrame(animate);
}
function Stop() {
    started = false;
}
function drawcircle(x, y, r, sa, ea, color) {
    context.beginPath();
    context.strokeStyle = "black";
    context.fillStyle = color;
    context.arc(x, y, r, sa, ea);
    context.fill();
    context.strokeStyle = "black";
    context.lineWidth = 3;
    context.stroke();
}
function mouseevt(x, y) {
    var r1;
    var r2;
    r1 = Math.sqrt(Math.pow((x - 50), 2) + Math.pow((y - 300), 2));
    r2 = Math.sqrt(Math.pow((x - 50), 2) + Math.pow((y - 350), 2));
    context.fillStyle = "black";
    context.font = "10px Arial";
    if (r1 < 20) {
        if (color1 == "grey") {
            color1 = "green";
            context.fillText("OFF", 40, 300, 35);
        }
        else {
            color1 = "grey";
            context.fillText("ON", 40, 300, 35);
        }
        drawall();
    }
    if (r2 < 20) {
        if (color2 == "grey")
            color2 = "green";
        else
            color2 = "grey";
        drawall();
    }
    if (color1 == "green" && color2 == "green") {
        if (!started) {
            started = true;
            l1.draw("green");
            l2.draw("green");
            animate();
        }
    }
    else {
        started = false;
    }
}
function gate(color) {
    context.beginPath();
    context.strokeStyle = color;
    context.moveTo(70, 300);
    context.lineTo(150, 300);
    context.moveTo(70, 350);
    context.lineTo(150, 350);
    context.moveTo(180 + 35, 325);
    context.lineTo(300, 325);
    context.stroke();
    context.beginPath();
    context.strokeStyle = "black";
    context.moveTo(150, 290);
    context.lineTo(150, 360);
    context.moveTo(150, 290);
    context.lineTo(180, 290);
    context.moveTo(150, 360);
    context.lineTo(180, 360);
    context.moveTo(180, 290);
    context.arc(180, 325, 35, -Math.PI / 2, Math.PI / 2);
    context.stroke();
}
//# sourceMappingURL=app.js.map